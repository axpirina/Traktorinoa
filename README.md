## Ongietorri Traktorinoa proiektura

GitHub repositorian aurkituko duzu informazio xehetuagoa (https://github.com/axpirina/Traktorinoa) ..

Whenever you commit to this repository, GitHub Pages will run [Jekyll](https://jekyllrb.com/) to rebuild the pages in your site, from the content in your Markdown files.

### Kodea

Jarraian Traktorino 1.0 bertsioaren kodea luzatzen dizuet.

```markdown
Syntax highlighted code block

# Header 1
## Header 2
### Header 3

- Bulleted
- List

1. Numbered
2. List

**Bold** and _Italic_ and `Code` text

[Link](url) and ![Image](src)
```

For more details see [GitHub Flavored Markdown](https://guides.github.com/features/mastering-markdown/).

### Simulazioak

Proiektuaren simulazioak hemen aurkituko dituzue [simulazioak](https://www.tinkercad.com/#/dashboard?type=all&collection=projects&id=hl08GTzHE5j) 

### Kontaktua

Egielaren kontaktua (axpirina@oteitzalp.com) edo GitHub kontaktua [contact support](https://github.com/contact).
